PROJECT = gitlab-ci-test
VERSION ?= $(shell git rev-parse --short HEAD)

install:
	go install .

build:
	CGO_ENABLED=0 GOOS=linux go build -a -tags netgo -ldflags '-w' -o bin/$(PROJECT)-$(VERSION) .

test:
	go test -v .